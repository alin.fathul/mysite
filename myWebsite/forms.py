from django import forms
from django.forms import fields

from .models import Matkul, Kegiatan, Peserta
from django.forms.models import ModelForm

class MatkulForm(ModelForm):
    class Meta:
        model = Matkul
        fields = ['nama', 'dosen', 'sks', 'deskripsi', 'tahun', 'ruangKelas']

class KegiatanForm(ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['nama']

class PesertaForm(ModelForm):
    class Meta:
        model = Peserta
        fields = ['nama', 'kegiatan']