from django.db import models

class Matkul(models.Model):
    nama = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    sks = models.IntegerField()
    deskripsi = models.TextField()
    tahun = models.CharField(max_length=30)
    ruangKelas = models.CharField(max_length=30)

    def __str__(self):
        return self.nama

class Kegiatan(models.Model):
    nama = models.CharField(max_length=30)

    def __str__(self):
        return self.nama

class Peserta(models.Model):
    nama = models.CharField(max_length=30)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)

    def __str__(self):
        return self.nama

    class Meta:
        ordering = ['nama']

# Create your models here.
