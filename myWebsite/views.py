from django.shortcuts import get_object_or_404, redirect, render
from django.http import HttpResponse
from .models import Matkul, Kegiatan, Peserta
from .forms import MatkulForm, KegiatanForm, PesertaForm

# Create your views here.
def index(request):
    if request.method == 'GET':
        data = request.GET.get('matkul')
        matkul = Matkul.objects.filter(nama = data)
        matkul.delete()
        
        
    matkul = Matkul.objects.all()
    return render(request, 'myWebsite/index.html', {'matkul' : matkul})

def detail(request):
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
    else:
        form = MatkulForm()

    return render(request, 'myWebsite/detail.html', {'form': form})

def tambahKegiatan(request):
    if request.method == 'POST':
        form = KegiatanForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('/kegiatan')
    else:
        form = KegiatanForm()

    return render(request, 'myWebsite/tambah-kegiatan.html', {'form': form})

def kegiatan(request):
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    return render(request, 'myWebsite/kegiatan.html', {'kegiatan' : kegiatan, 'peserta' : peserta})

def tambahPeserta(request):
    if request.method == 'POST':
        form = PesertaForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('/kegiatan')
    else:
        form = PesertaForm()

    return render(request, 'myWebsite/tambah-peserta.html', {'form': form})