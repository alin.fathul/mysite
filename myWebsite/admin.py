from django.contrib import admin
from .models import Matkul, Kegiatan, Peserta

admin.site.register(Matkul)
admin.site.register(Kegiatan)
admin.site.register(Peserta)
# Register your models here.
