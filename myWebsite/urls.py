from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('detail/', views.detail, name='detail'),
    path('tambah-kegiatan/', views.tambahKegiatan, name='tambah-kegiatan'),
    path('tambah-peserta/', views.tambahPeserta, name='tambah-peserta'),
    path('kegiatan/', views.kegiatan, name='kegiatan')
]