from django.test import TestCase
from .models import Matkul, Kegiatan, Peserta
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import time
# Create your tests here.

class KegiatanTestCase(TestCase):
    def setUp(self):
        k1 = Kegiatan.objects.create(nama="k1")
        k2 = Kegiatan.objects.create(nama="k2")
        k3 = Kegiatan.objects.create(nama="k3")
        k4 = Kegiatan.objects.create(nama="k4")
        k5 = Kegiatan.objects.create(nama="k5")

        p1 = Peserta.objects.create(nama="p1", kegiatan=k1)
        p2 = Peserta.objects.create(nama="p2", kegiatan=k2)
        p3 = Peserta.objects.create(nama="p3", kegiatan=k3)
        p4 = Peserta.objects.create(nama="p4", kegiatan=k4)
        p5 = Peserta.objects.create(nama="p5", kegiatan=k5)

    def test_peserta_di_database(self):
        k1 = Kegiatan.objects.get(nama="k1")
        k2 = Kegiatan.objects.get(nama="k2")
        k3 = Kegiatan.objects.get(nama="k3")
        k4 = Kegiatan.objects.get(nama="k4")
        k5 = Kegiatan.objects.get(nama="k5")

        p1 = Peserta.objects.get(nama="p1")
        p2 = Peserta.objects.get(nama="p2")
        p3 = Peserta.objects.get(nama="p3")
        p4 = Peserta.objects.get(nama="p4")
        p5 = Peserta.objects.get(nama="p5")

        self.assertEqual(k1.peserta_set.get(nama="p1").nama, 'p1')
        self.assertEqual(k2.peserta_set.get(nama="p2").nama, 'p2')
        self.assertEqual(k3.peserta_set.get(nama="p3").nama, 'p3')
        self.assertEqual(k4.peserta_set.get(nama="p4").nama, 'p4')
        self.assertEqual(k5.peserta_set.get(nama="p5").nama, 'p5')

#class FunctionalTest(StaticLiveServerTestCase):
#    def setUp(self):
#        self.driver = webdriver.Chrome(ChromeDriver().install())
#
#    def tearDown(self):
#        self.driver.close()
#
#    def test_kegiatan_peserta_functional(self):
#        driver = self.driver
#        driver.get(self.live_server_url)
#        time.sleep(1)
#        driver.find_element_by_link_text("Tambah Kegiatan").click()
#        time.sleep(1)
#        driver.find_element_by_name("nama").send_keys("k1")
#        time.sleep(1)
#        driver.find_element_by_name("kegiatan").click()
#        time.sleep(1)
#        driver.find_element_by_link_text("Tambah Peserta").click()
#        time.sleep(1)
#        driver.find_element_by_name("nama").send_keys("p1")
#        time.sleep(1)
#        Select(driver.find_element_by_name("kegiatan")).select_by_visible_text("k1")
#        time.sleep(1)
#        driver.find_element_by_name("peserta").click()
#        time.sleep(1)

#        self.assertEqual(Kegiatan.objects.get(nama="k1").peserta_set.get(nama="p1").nama, 'p1')


